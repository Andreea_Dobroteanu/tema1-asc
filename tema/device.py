"""
This module represents a device.

Computer Systems Architecture Course
Assignment 1
March 2016

Dobroteanu Andreea, 331CA
"""

from threading import Event, Thread, Lock
from multiprocessing.dummy import Pool as ThreadPool
from collections import namedtuple
from barrier import ReusableBarrierCond


class Device(object):
    """
    Class that represents a device.
    """

    def __init__(self, device_id, sensor_data, supervisor):
        """
        Constructor.

        @type device_id: Integer
        @param device_id: the unique id of this node; between 0 and N-1

        @type sensor_data: List of (Integer, Float)
        @param sensor_data: a list containing (location, data) as measured by
            this device

        @type supervisor: Supervisor
        @param supervisor: the testing infrastructure's control and validation
            component
        """
        self.device_id = device_id
        self.sensor_data = sensor_data
        self.supervisor = supervisor
        self.script_received = Event()
        self.scripts = []
        self.thread = DeviceThread(self)

        # each location requires a lock
        self.locks = []

        self.barrier = None

    def __str__(self):
        """
        Pretty prints this device.

        @rtype: String
        @return: a string containing the id of this device
        """
        return "Device %d" % self.device_id

    def setup_devices(self, devices):
        """
        Setup the devices before simulation begins.

        @type devices: List of Device
        @param devices: list containing all devices
        """
        # we don't need no stinkin' setup
        # unfortunately, we do :|

        # need barrier to let every thread reach the timepoint
        # before going further
        barrier = ReusableBarrierCond(len(devices))

        # the first device is the master; it will add the locks & barriers,
        # and start the threads
        if self.device_id == 0:
            # maximum number of locations from all devices
            locations_no = 0

            # each location should should have a lock (a device can
            # have more locations), so we have to find the maximum
            # number of locations from every device
            for device in devices:
                locations_no = max([max(device.sensor_data), locations_no])

            for device in devices:
                # each device will have a barrier
                device.barrier = barrier

                # assigning locks for each location on every device
                for _ in xrange(locations_no + 1):
                    lock_location = Lock()
                    device.locks.append(lock_location)

                # starts the master thread of each device
                device.thread.start()

    def assign_script(self, script, location):
        """
        Provide a script for the device to execute.

        @type script: Script
        @param script: the script to execute from now on at each timepoint;
            None if the current timepoint has ended

        @type location: Integer
        @param location: the location for which the script is interested in
        """
        if script is not None:
            # add the received script to the list
            self.scripts.append((script, location))
        else:
            # notify that the script has been received
            self.script_received.set()

    def get_data(self, location):
        """
        Returns the pollution value this device has for the given location.

        @type location: Integer
        @param location: a location for which obtain the data

        @rtype: Float
        @return: the pollution value
        """
        return self.sensor_data[location] if location in self.sensor_data \
            else None

    def set_data(self, location, data):
        """
        Sets the pollution value stored by this device for the given location.

        @type location: Integer
        @param location: a location for which to set the data

        @type data: Float
        @param data: the pollution value
        """
        if location in self.sensor_data:
            self.sensor_data[location] = data

    def shutdown(self):
        """
        Instructs the device to shutdown (terminate all threads). This method
        is invoked by the tester. This method must block until all the threads
        started by this device terminate.
        """
        self.thread.join()


class DeviceThread(Thread):
    """
    Clasa ce implementeaza thread-ul master al device-ului.
    """

    def __init__(self, device):
        """
        Constructor.

        @type device: Device
        @param device: device-ul ce detine acest thread
        """
        Thread.__init__(self, name="Device Thread %d" % device.device_id)
        self.device = device

        import multiprocessing
        self.cores_no = multiprocessing.cpu_count()
        # this is kind of an object builder; its instances will store
        # script useful info
        self.script_helper = namedtuple("script_helper", "device script "
                                                         "location neighbours")

    def run(self):
        while True:
            # scripts info array for the workers pool
            scripts_info = []
            # get the current neighbourhood
            neighbours = self.device.supervisor.get_neighbours()
            if neighbours is None:
                break

            # wait for all scripts for this timepoint to arrive
            self.device.script_received.wait()
            self.device.script_received.clear()

            # populate scripts info array
            for (script, location) in self.device.scripts:
                scripts_info.append(self.script_helper(self.device, script,
                                                       location, neighbours))

            # create pool of threads, for solving scripts
            if len(scripts_info):
                pool = ThreadPool(self.cores_no)
                pool.map(self.solve_script, scripts_info)
                pool.close()
                pool.join()

            # wait for all threads to in order to get to the next timepoint
            self.device.barrier.wait()

    def solve_script(self, script_info):
        """
        Solves a script

        @param script_info: object with script useful info:
            location, script and neighbours
        """
        self.device.locks[script_info.location].acquire()
        script_data = []
        # collect data from current neighbours
        for device in script_info.neighbours:
            data = device.get_data(script_info.location)
            if data is not None:
                script_data.append(data)
        # add our data, if any
        data = self.device.get_data(script_info.location)
        if data is not None:
            script_data.append(data)

        if script_data:
            # run script on data
            result = script_info.script.run(script_data)

            # update data of neighbours, hope no one is updating
            # at the same time
            for device in script_info.neighbours:
                device.set_data(script_info.location, result)
            # update our data, hope no one is updating at the same time
            self.device.set_data(script_info.location, result)

        self.device.locks[script_info.location].release()
